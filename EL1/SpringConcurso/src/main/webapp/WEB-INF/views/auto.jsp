
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Auto</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<!-- Font Awesome Stylesheets -->
<link rel="stylesheet"
	href="<c:url value="/resources/css/font-awesome.min.css"/>"
	type="text/css">
	
<style>
	.error {
		color: red;
	}
</style>

</head>
<body>
	<div class="container mt-2">
		<div class="card">
			<div class="card-header bg-primary text-white text-center p-1">
				<h5>Formulario de Auto</h5>
			</div>
			<div class="card-body">
				<form action="guardar" method="post" id="frmRegistro">
				<input type="hidden" id="codigo" name="codigo" value="0">
					<div class="row">
						<div class="form-group col-md-6">
							<label>Descripci�n (*)</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fa fa-car"></i></span>
								</div>
								<input type="text" class="form-control" id="descripcion" name="descripcion" />
							</div>
						    <span class="error" for="descripcion"></span>
						</div>
						<div class="form-group col-md-6">
							<label>Precio (*)</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><strong>S/.</strong></span>
								</div>
								<input type="text" class="form-control" id="precio"
									name="precio" />
							</div>
						</div>

						<div class="form-group col-md-6">
							<label>Stock (*)</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i
										class="fa fa-address-card"></i></span>
								</div>
								<input type="text" class="form-control" id="stock" name="stock" />
							</div>
						</div>
						<div class="form-group col-md-6">
							<label>Marca (*)</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fa fa-car"></i></span>
								</div>
								<select class="form-control" id="codMarca" name="codMarca">
									<option value="">[seleccione marca]</option>
								</select>
							</div>
						</div>
						<div class="col-md-12 text-center">
							<button type="submit" class="btn btn-success">
								<i class='fa fa-save'></i> REGISTRAR
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--  -->

	<script src="http://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
		
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
		
	<script>
		$(function() {
			listarMarcas();
			
			$("#frmRegistro").validate({
				   rules: {
				     descripcion: {
				    	 required: true,
				    	 maxlength: 100
				     },
				     precio: {
				    	 required: true,
				    	 maxlength: 10,
				    	 number: true
				     },
				     stock: {
				    	 required: true,
				    	 maxlength: 6,
				    	 digits: true
				     },
				     codMarca: {
				    	 required: true
				     }
				   },
				   messages: {
					   descripcion: {
						   required: "Debe ingresar descripci�n",
						   maxlength: "Maximo {0} caracteres"
					   },
					   precio: {
						   required: "Debe ingresar precio",
						   number: "Debe ingresar un precio v�lido"
					   },
					   stock: {
						   required: "Debe ingresar cantidad",
						   digits: "Debe ingresar un entero v�lido"
					   },
					   codMarca: {
						   required: "Debe seleccionar marca"
					   }
				   },
				   
				   errorElement: 'span',
			        errorClass: 'error',
			        errorPlacement: function (error, element) {
			            if (element.parent('.input-group').length) {
			                error.insertAfter(element.parent());
			            } else {
			                error.insertAfter(element);
			            }
			        },
			        showErrors: function (errorMap, errorList) {
			            let errors = this.numberOfInvalids();
			            if (errors) {
			                let message = errors == 1
			                  ? 'Your form has 1 error'
			                  : 'Your form has ' + errors + ' errors.';
			                message = message + ' Please, fix then.'
			                $("#error span").empty().html(message);
			                $("#error").show();
			            } else {
			                $("#error").hide();
			            }
			            this.defaultShowErrors();
			        }
			 });
		});
		
		function listarMarcas() {
			$.ajax({
				'url': '../marca/listar',
				'type': 'get',
				
			})
			.done(function(data, status) {
				data.map(function(e, i) {
					$("#codMarca").append($('<option>', {
						value : e.codigo,
						text : e.nombre
					}));
				});
			})
			.fail(function(data, status) {
				console.log("Ha ocurrido un error")
			});
		}
	</script>

</body>
</html>