package net.spring.concurso.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import net.spring.concurso.entity.Auto;
import net.spring.concurso.entity.Marca;
import net.spring.concurso.service.AutoService;

@Controller
@RequestMapping(value = "/auto")
public class AutoController {
	
	@Autowired
	private AutoService service;
	
	@RequestMapping(name = "/")
	public String index() {
		return "auto";
	}
	
	@RequestMapping(value = "/guardar")
	public String save
	(
		@RequestParam("codigo") int codigo,
		@RequestParam("descripcion") String descripcion,
		@RequestParam("precio") double precio,
		@RequestParam("stock") int stock,
		@RequestParam("codMarca") int codMarca
	)
	{
		try {
			Auto entidad = new Auto();
			
			Marca marca = new Marca();
			marca.setCodigo(codMarca);
			
			entidad.setCodigo(codigo);
			entidad.setDescripcion(descripcion);
			entidad.setPrecio(precio);
			entidad.setStock(stock);
			entidad.setMarca(marca);
			
			service.save(entidad);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "auto";
	}
}
