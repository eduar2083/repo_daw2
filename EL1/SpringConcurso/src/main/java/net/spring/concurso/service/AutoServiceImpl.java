package net.spring.concurso.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.spring.concurso.dao.AutoDAO;
import net.spring.concurso.entity.Auto;

@Service
public class AutoServiceImpl implements AutoService {
	
	@Autowired
	private AutoDAO dao;

	@Override
	public void save(Auto entidad) {
		dao.save(entidad);
	}
}
