package net.spring.concurso.dao;

import net.spring.concurso.entity.Auto;

public interface AutoDAO {
	public void save(Auto entidad);
}
