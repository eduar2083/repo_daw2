package net.spring.concurso.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.spring.concurso.dao.MarcaDAO;
import net.spring.concurso.entity.Marca;

@Service
public class MarcaServiceImpl implements MarcaService {

	@Autowired
	private MarcaDAO dao;
	
	@Override
	public List<Marca> listar() {
		return dao.listar();
	}

}
