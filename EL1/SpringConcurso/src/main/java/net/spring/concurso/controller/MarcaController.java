package net.spring.concurso.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import net.spring.concurso.entity.Marca;
import net.spring.concurso.service.MarcaService;

@Controller
@RequestMapping(value = "/marca")
public class MarcaController {

	@Autowired
	private MarcaService service;

	@RequestMapping(value = "/listar")
	@ResponseBody
	public List<Marca> listAll() {
		return service.listar();
	}
}
