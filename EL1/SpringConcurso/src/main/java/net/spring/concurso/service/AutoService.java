package net.spring.concurso.service;

import org.springframework.stereotype.Service;

import net.spring.concurso.entity.Auto;

@Service
public interface AutoService {
	public void save(Auto entidad);
}
