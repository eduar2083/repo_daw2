package net.spring.concurso.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.spring.concurso.entity.Auto;

@Repository
public class AutoDAOImpl implements AutoDAO {
	
	@Autowired
	private SessionFactory factory;

	@Override
	@Transactional
	public void save(Auto entidad) {
		Session sesion = factory.getCurrentSession();
		
		try {
			sesion.saveOrUpdate(entidad);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
